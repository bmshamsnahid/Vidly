const express = require('express');
const router = express.Router();
const { Customer, validateCustomer } = require('../models/customer');

router.get('/', async (req, res, next) => {
    const customers = await Customer.find().sort('name');
    res.send(customers);
});

router.post('/', async (req, res, next) => {
    const { error } = validateCustomer(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    let customer = new Customer({
        name: req.body.name,
        isGold: req.body.isGold,
        phone: req.body.phone
    });

    await customer.save();

    return res.send(customer);
});

router.put('/:id', async (req, res, next) => {
    const { error } = validateCustomer(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const customer = await customer.findByIdAndUpdate(req.params.id, {
        name: req.body.name,
        isGold: req.body.isGold,
        phone: req.body.phone
    }, { new: true });
    if (!customer) {
        return res.status(404).send('The customer with the given id was not found.');
    }

    return res.send(customer);
});

router.delete(':/id', async (req, res) => {
    const customer = await Customer.findByIdAndRemove(req.params.id);
    if (!customer) {
        return res.status(404).send('The customer with the given id was not found.');
    }

    return res.send(customer);
});

router.get('/:id', async (req, res, next) => {
    const customer = await Customer.findById(req.params.id);
    if (!customer) {
        return res.status(404).send('The customer with the given id was not found.');
    }

    return res.send(customer);
});

module.exports = router;