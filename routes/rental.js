const express = require('express');
const router = express.Router();
const { Rental, validateRental } = require('../models/rental');
const { Customer } = require('../models/customer');
const { Movie } = require('../models/movie');
const Fawn = require('fawn');
const mongoose = require('mongoose');

Fawn.init(mongoose);

router.get('/', async (req, res, next) => {
    const rentals = await Rental.find().sort('-dateOut');
    return res.send(rentals);
});

router.post('/', async (req, res, next) => {
    const { error } = validateRental(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    const customer = await Customer.findById(req.body.customerId);
    if (!customer) {
        return res.status(404).send('Invalid customer id');
    }

    const movie = await Movie.findById(req.body.movieId);
    if (!movie) {
        return res.status(404).send('Invalid movie id');
    }

    if (movie.numberInStock === 0) {
        return res.status(400).send('Movies are out of stock');
    }

    let rental = new Rental({
        customer: {
            _id: customer._id,
            name: customer.name,
            phone: customer.phone
        },
        movie: {
            _id: movie._id,
            title: movie.title,
            dailyRentalRate: movie.dailyRentalRate
        }
    });

    try {
        new Fawn.Task()
            .save('rentals', rental)
            .update('movies', { _id: movie._id }, {
                $inc: { numberInStock: -1 }
            })
            .run();
        return res.send(rental);
    } catch (ex) {
        console.log(ex);
        res.status(500).send('Internal Error');
    }
});

router.put('/:id', async (req, res, next) => {
    return res.send('Functionality under development');
});

router.delete('/:id', async (req, res, next) => {
    return res.send('Functionality under development');
});

router.get('/:id', async (req, res, next) => {
    const rental = await Rental.findById(req.params.id);
    if (!rental) {
        return res.status(404).send('The rental with the given id was not found.');
    }
    res.send(rental);
});

module.exports = router;