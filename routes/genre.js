const auth = require('../middleware/auth');
const admin = require('../middleware/admin');
const { Genre, validateGenre } = require('../models/genre');
const express = require('express');
const router = express.Router();

router.get('/', async (req, res) => {
    const genres = await Genre.find().sort('name');
    return res.send(genres);
});

router.post('/', auth, async (req, res) => {
    const { error } = validateGenre(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }

    let genre = new Genre({ name: req.body.name });
    genre = await genre.save();

    return res.send(genre);
});

router.put('/:id', async (req, res) => {
    const { error } = validateGenre(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }
    const genre = await Genre.findByIdAndUpdate(req.params.id, { name: req.body.name }, { new: true });
    return res.send(genre);
});

router.delete('/:id', async (req, res) => {
    const genre = await Genre.findByIdAndRemove(req.params.id);
    if (!genre) {
        return res.status(404).send('The genre with the given id was not found.');
    }
    return res.send(genre);
});

router.get(':/id', async (req, res) => {
    const genre = Genre.findById(req.params.id);
    if (!genre) {
        return res.status(404).send('The genre with the given id was not found.');
    }
    return res.send(genre);
});

module.exports = router;