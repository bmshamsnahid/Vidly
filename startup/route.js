const express = require('express');
const morgan = require('morgan');
const error = require('../middleware/error');
const user = require('../routes/user');
const auth = require('../routes/auth');
const genre = require('../routes/genre');
const movie = require('../routes/movie');
const customer = require('../routes/customer');
const rental = require('../routes/rental');

module.exports = (app) => {
    app.use(express.json());
    app.use(morgan('dev'));
    app.use('/api/users', user);
    app.use('/api/auth', auth);
    app.use('/api/genres', genre);
    app.use('/api/movies', movie);
    app.use('/api/customers', customer);
    app.use('/api/rentals', rental);
    app.use(error);
};