const config = require('config');
const winston = require('winston');

module.exports = () => {
    if (!config.get('jwtPrivateKey')) {
        winston.error('Fatal Error: jwtPrivate key is not defined.');
        throw new Error('Fatal Error: jwtPrivate key is not defined.');
    }
};