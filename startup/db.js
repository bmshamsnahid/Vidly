const mongoose = require('mongoose');
const winston = require('winston');
const config = require('config');
const uri = config.get('databaseUri');

const options = {
    useNewUrlParser: true,
    autoIndex: true, //building indexes
    reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
    reconnectInterval: 500, // Reconnect every 500ms
    poolSize: 10, // Maintain up to 10 socket connections
    // If not connected, return errors immediately rather than waiting for reconnect
    bufferMaxEntries: 0,
    connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
    socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
    family: 4 // Use IPv4, skip trying IPv6
};

module.exports = () => {
    mongoose.connect(uri, options)
        .then(() => winston.info('Connected to database: ' + uri));
};