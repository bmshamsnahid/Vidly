const winston = require('winston');
const config = require('config');
const uri = config.get('databaseUri');

require('winston-mongodb');
require('express-async-errors');

module.exports = () => {

    winston.handleExceptions(new winston.transports.File({ filename: 'uncaughtExceptions.log' }));

    process.on('unhandledException', (exception) => {
        throw exception;
    });

    winston.add(winston.transports.File, { filename: 'logfile.log' });
    winston.add(winston.transports.MongoDB, {
        db: uri
    });
};